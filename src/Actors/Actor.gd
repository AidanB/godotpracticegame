extends KinematicBody2D
class_name Actor

const FLOOR_NORMAL: = Vector2.UP

export var speed: = Vector2(300.0, 1000.0)
export var gravity: = 3000.0
export var hitpoints: = 100.0
export var curHitpoints: = 100.0
var direction: = Vector2(1.0, 0.0)
var _velocity: = Vector2.ZERO

#sets the direction that the sprite should be facing
func set_sprite_direction(dir: float, spriteName: String):
	if dir > 0.0:
		get_node(spriteName).set_flip_h(false)
	elif dir < 0.0:
		get_node(spriteName).set_flip_h(true)
	return dir
	
#calculates the velocity of an actor
func calculate_move_velocity(linearVelocity: Vector2, spd: Vector2, dir: Vector2) -> Vector2:
	var newVelocity: = linearVelocity
	newVelocity.x = spd.x * dir.x
	newVelocity.y += gravity * get_physics_process_delta_time()
	if direction.y == -1.0:
		newVelocity.y = speed.y * dir.y
	return newVelocity
