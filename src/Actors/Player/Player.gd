extends Actor
class_name Player
const SPRITE = "playerSprite"

func _ready() -> void:
	pass # Replace with function body.

func _physics_process(delta):
	direction = get_direction()
	_velocity = calculate_move_velocity(_velocity, speed, direction)
	_velocity = move_and_slide(_velocity, FLOOR_NORMAL)

func get_direction() -> Vector2:
	var inputDirection = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	set_sprite_direction(inputDirection, SPRITE)
	return Vector2(inputDirection , 0.0)
	


