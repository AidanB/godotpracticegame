extends Actor
class_name Enemy

var aggro = false

#makes an enemy face the player when called
func face_player(sprite: String):
	if get_parent().get_node("Player").global_position.x > global_position.x:
		direction = Vector2(1.0, 0.0)
	else:
		direction = Vector2(-1.0, 0.0)
		
	set_sprite_direction(direction.x, sprite)	
	
	return direction
	
