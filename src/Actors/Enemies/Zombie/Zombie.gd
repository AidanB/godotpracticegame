extends Enemy
class_name Zombie

const SPRITE = "zombieSprite"

func _ready() -> void:
	speed = Vector2(0, 1000)
	set_physics_process(false)
	pass # Replace with function body.

func _physics_process(delta):
	if aggro == true:
		direction = face_player(SPRITE)
	_velocity = calculate_move_velocity(_velocity, speed, direction)
	_velocity = move_and_slide(_velocity, FLOOR_NORMAL)

func _on_aggroRadius_body_entered(body: Node):
	if body is Player:
		aggro = true
		speed = Vector2(75, 1000)
