extends Particles2D


onready var timer:= $Timer
onready var light:= $Light2D
onready var rng = RandomNumberGenerator.new()
func _ready() -> void:
	randomize()

#creates a flickering effect for the light
func _on_Timer_timeout() -> void:
	var rand_amt = rng.randf_range(1.5, 1.6)
	light.energy = rand_amt
	timer.start(rand_amt/20)
