shader_type canvas_item;

uniform vec3 color = vec3(0.5, 0.5, 0.5);
uniform int octaves = 4;
uniform float opacity = 0.5;
uniform vec2 direction = vec2(0.8, 0.0);
uniform float aspect_ratio = 0.5;
uniform vec2 tiled_factor = vec2(5.0, 5.0);

//random function used to randomzie alpha values
float random(vec2 coordinates){
	return fract(sin(dot(coordinates, vec2(33, 44)) * 100.0) * 100.0);
}
//noise function that is used to normalize the alpha values of the pixels
float noise(vec2 coord){
	vec2 i = floor(coord);
	vec2 f = fract(coord);
	
	float a= random(i);
	float b= random(i + vec2(1.0, 0.0));
	float c= random(i + vec2(0.0, 1.0));
	float d= random(i + vec2(1.0, 1.0));
	
	vec2 cubic = f * f * (3.0 - 2.0 * f);
	
	return mix(a, b, f.x) + (c - a) * f.y * (1.0 - f.x) + (d -b) * f.x *f.y;
}
//brownian motion function used to add increased detail to the fog effect
float fbm(vec2 coord){
	float v = 0.0;
	float s = 0.5;
	
	for(int i = 0; i < octaves; i++){
		v += noise(coord) * s;
		coord *= 2.0;
		s *= 0.5;
	}
	return v;
}

void fragment(){
	//allows for the modification of the aspect ratio
	vec2 coord = UV * tiled_factor;
	coord.y *=aspect_ratio;
	coord *= 20.0;
	
	//adds movement and distortion effects
	vec2 motion = vec2(fbm(coord + TIME * direction));
	
	//output
	COLOR = vec4(color, fbm(coord + motion) * opacity);
}