tool
extends Sprite

#allows the the readjustment of the fogs aspect ratio
func calculate_aspect_ratio():
	material.set_shader_param("aspect_ratio", scale.y /scale.x)
